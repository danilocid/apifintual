<?php
include 'controller.php';

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/dashboard.css">
    <link rel="stylesheet" type="text/css" href="DataTables/datatables.min.css"/>
    <title>Api Fintual V2</title>
</head>
<body>


<nav class="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow">
  <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="#">Api Fintual</a>
  </nav>

<div class="container-fluid">
  <div class="row">
    <nav class="col-md-2 d-none d-md-block bg-light sidebar">
      <div class="sidebar-sticky">
        <ul class="nav flex-column">
            <li class="nav-item">
              <a class="nav-link active" href="index">
                <span data-feather="home"></span>
                Dashboard <span class="sr-only">(current)</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link " href="casanueva">
                <span data-feather="shopping-cart"></span>
                Casa nueva
              </a>
            </li>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="apv">
                <span data-feather="shopping-cart"></span>
               APV
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="depositos">
                <span data-feather="shopping-cart"></span>
               Depositos
              </a>
            </li>
            
          </ul>

        
        
      </div>
    </nav>
    <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
      <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Resumen</h1>
        
      </div>

      <canvas class="my-4 w-100" id="myChart" width="900" height="380"></canvas>

      <h2>Detalle</h2>
      <div class="table-responsive">
        <table class="table table-bordered table-striped" id="apv">
          <thead>
            <tr>
              <th>N°</th>
              <th>Inversion</th>
              <th>Monto</th>
              <th>Invertido</th>
              <th>Ganancia</th>
              <th>Inversion</th>
              <th>Monto</th>
              <th>Invertido</th>
              <th>Ganancia</th>
              <th>Total invertido</th>
              <th>Total ganancia</th>
              <th>Fecha</th>
            </tr>
          </thead>
          <tbody>
          <?php
              
              class DatosOrdenados
              {
                public $id;
                public $goal;
                public $monto;
                public $ganancia;
                public $invertido;
                public $id2;
                public $goal2;
                public $monto2;
                public $ganancia2;
                public $invertido2;
                public $fecha;
                    }
                    $dat = new DB();
              $data = $dat->GetDatos();
              //print_r($data);
              
              setlocale(LC_TIME, 'es_ES.UTF-8');
              
              $contador = count($data);
               $datosordenados  = new DatosOrdenados();
               $datosord = array();
               
               for ($i=0; $i < $contador; $i = $i + 2) { 
                 $datosordenados  = new DatosOrdenados();
                 $datosordenados->id = $data[$i]->id;
                 $datosordenados->goal = $data[$i]->goal;
                 $datosordenados->monto = $data[$i]->monto;
                 $datosordenados->ganancia = $data[$i]->monto - $data[$i]->invertido;
                 $datosordenados->invertido = $data[$i]->invertido;
                 $datosordenados->id2 = $data[$i+1]->id;
                 $datosordenados->goal2 = $data[$i+1]->goal;
                 $datosordenados->monto2 = $data[$i+1]->monto;
                 $datosordenados->ganancia2 = $data[$i+1]->monto - $data[$i+1]->invertido;
                 $datosordenados->invertido2 = $data[$i+1]->invertido;
                 $datosordenados->fecha = $data[$i]->fecha;
                 array_push($datosord, $datosordenados);
               }
               //print_r($datosord);
              
               $contador = 1;
              $apvmontostring = "";
              $apvinvertidostring = "";
              $casanuevamontostring = "";
              $casanuevainvertidostring = "";
              $fechas = "";
             
              foreach($datosord as $d) {
                  
                echo '<tr>
                <td>'.$contador.'</td>
                <td>'.$dat->GetGoalPorId($d->goal).'</td>
                <td> $'.number_format($d->monto, 0, ',', '.').'</td>
                <td> $'.number_format($d->invertido, 0, ',', '.').'</td>
                <td>'.number_format(($d->monto-$d->invertido), 0, ',', '.').'</td>
                <td>'.$dat->GetGoalPorId($d->goal2).'</td>
                <td> $'.number_format($d->monto2, 0, ',', '.').'</td>
                <td> $'.number_format($d->invertido2, 0, ',', '.').'</td>
                <td>'.number_format(($d->monto2-$d->invertido2), 0, ',', '.').'</td>
                <td>'.number_format($d->invertido2 + $d->invertido, 0, ',', '.').'</td>
                <td>'.number_format(($d->monto2-$d->invertido2)+($d->monto-$d->invertido), 0, ',', '.').'</td>';
                echo '<td>'.$d->fecha.'</td>';
                                    
                      echo '</tr>
                      ';
                
                $fechas = $fechas . "'".(date("d-m-Y",strtotime($d->fecha))."'".',');
                $apvmontostring = $apvmontostring . "'".number_format($d->monto2, 0, ',', '')."'".',';
                $apvinvertidostring = $apvinvertidostring . "'".number_format($d->invertido2, 0, ',', '')."'".',';
                $casanuevamontostring = $casanuevamontostring . "'".number_format($d->monto, 0, ',', '')."'".',';
                $casanuevainvertidostring = $casanuevainvertidostring . "'".number_format($d->invertido, 0, ',', '')."'".',';
                $contador++;
              }
              $fechas = trim($fechas, ',');
              $apvmontostring = trim($apvmontostring, ',');
              $apvinvertidostring = trim($apvinvertidostring, ',');
              $casanuevamontostring = trim($casanuevamontostring, ',');
              $casanuevainvertidostring = trim($casanuevainvertidostring, ',');
              
              
                ?>
            
          </tbody>
        </table>
      </div>
    </main>
  </div>
</div>




    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="js/bootstrap.js"></script>
    
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/feather-icons/4.9.0/feather.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.min.js"></script>
    <script type="text/javascript" src="DataTables/datatables.min.js"></script>
    
    <script>
        /* globals Chart:false, feather:false */

(function () {
  'use strict'

  feather.replace()

  // Graphs
  var ctx = document.getElementById('myChart')
  // eslint-disable-next-line no-unused-vars
  var myChart = new Chart(ctx, {
    type: 'line',
    data: {
      labels: [
        <?php
        echo $fechas;
        ?>
      ],
      datasets: [{
        label: 'APV invertido',
        data: [
          <?php
        echo $apvinvertidostring;
        ?>
        ],
        lineTension: 0,
        backgroundColor: 'transparent',
        borderColor: '#f6ddcc',
        borderWidth: 2,
        pointBackgroundColor: '#f6ddcc'},
        {
        label: 'APV monto',
        data: [
          <?php
        echo $apvmontostring;
        ?>
        ],
        lineTension: 0,
        backgroundColor: 'transparent',
        borderColor: '#e67e22',
        borderWidth: 2,
        pointBackgroundColor: '#e67e22'},
        {
        label: 'Casanueva invertido',
        data: [
          <?php
        echo $casanuevainvertidostring;
        ?>
        ],
        lineTension: 0,
        backgroundColor: 'transparent',
        borderColor: '#007bff',
        borderWidth: 2,
        pointBackgroundColor: '#007bff'},
        {
        label: 'Casanueva monto',
        data: [
          <?php
        echo $casanuevamontostring;
        ?>
        ],
        lineTension: 0,
        backgroundColor: 'transparent',
        borderColor: '#5dade2',
        borderWidth: 2,
        pointBackgroundColor: '#5dade2'
      }]
    },
    options: {
      scales: {
        yAxes: [{
          ticks: {
            beginAtZero: false
          }
        }]
      },
      legend: {
        display: true
      }
    }
  })
}())
    </script>
    <script>
      
  $(function () {
   
   $('#apv').DataTable({
     'paging'      : true,
     'lengthChange': false,
     'searching'   : false,
     'ordering'    : true,
     'info'        : true,
     'autoWidth'   : true,
     "order": [[ 8, "desc" ]]
   });
  })
</script>
</body>

</html>