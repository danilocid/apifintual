<?php
class Datos{
	public $id;
	public $goal;
	public $monto;
	public $invertido;
	public $fecha;
	

}
class DB
{
	private $host;
	private $db;
	private $user;
	private $password;

	public function __construct()
	{
		$this->host = 'localhost';
		$this->db = 'fintual';
		$this->user = 'root';
		$this->password = '';
	}

	public function connect()
	{
		try {
			$connection = "mysql:host=" . $this->host . ";dbname=" . $this->db;
			$options = [
				PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
				PDO::ATTR_EMULATE_PREPARES => false,
			];
			$pdo = new PDO($connection, $this->user, $this->password, $options);

			return $pdo;
		} catch (PDOException $e) {
			print_r('Error conenection: ' . $e->getMessage());
		}
    }
    
    public function SetDatos(){
		$query = $this->connect()->prepare('SELECT max(Fecha) FROM datos');
		$query->execute([]);
		foreach ($query as $a) {
			$max = $a[0];
			
		}
		$fecha = date("Y-m-d");
        if ($fecha == $max) {
           
        } else {

        $json = file_get_contents("https://fintual.cl/api/goals?user_token=EDFyJxckhKysafPWPzzD&user_email=danilo.cid.v%40gmail.com");
		$datos = json_decode($json, true);
 

		foreach ($datos as $d) {
		$id1 = $d[0]['id'];
		$monto1 = $d[0]['attributes']['nav'];
		$invertido1 = $this->GetInversionPorGoal($id1);
		$id2 = $d[1]['id'];
		$monto2 = $d[1]['attributes']['nav'];
		$invertido2 = $this->GetInversionPorGoal($id2);
		}
		try {
            $query = $this->connect()->prepare('INSERT into datos values(null,:id1, :monto1, :invertido1, now()),(null,:id2, :monto2, :invertido2, now());');
			$query->execute(['id1' => $id1, 'monto1' => $monto1, 'invertido1' => $invertido1, 'id2' => $id2, 'monto2' => $monto2, 'invertido2' => $invertido2]);
            return true;
        } catch (PDOException $e) {
			print_r('Error conenection: ' . $e->getMessage());
            return false;
		}
	}
	}
	
	public function GetDatos(){
		$query = $this->connect()->prepare('SELECT * FROM datos WHERE goal = 27362');
		$query->execute();
		$arraydatos = array();
		foreach ($query as $d) {
			$datos = new Datos;
			$datos->id = $d[0];
			$datos->goal = $d[1];
			$datos->monto = $d[2];
			$datos->fecha = $d[3];
			$datos->invertido = $d[4];
			
			array_push($arraydatos, $d);
		}
		return $arraydatos;
	}
	public function GetDatos2(){
		$query = $this->connect()->prepare('SELECT * FROM datos WHERE goal = 41679');
		$query->execute();
		$arraydatos = array();
		foreach ($query as $d) {
			$datos = new Datos;
			$datos->id = $d[0];
			$datos->goal = $d[1];
			$datos->monto = $d[2];
			$datos->fecha = $d[3];
			$datos->invertido = $d[4];
			
			array_push($arraydatos, $d);
		}
		return $arraydatos;
	}
	public function GetGoalPorId($goal){
		$query = $this->connect()->prepare('SELECT * FROM goals WHERE goal = :goal');
		$query->execute(['goal' => $goal]);
		foreach ($query as $g) {
			$nombre = $g[2];
		}
		return $nombre;
	}
	public function GetGoals(){
		$query = $this->connect()->prepare('SELECT * FROM goals');
		$query->execute();
		$arraygoals = array();
		foreach ($query as $g) {
			array_push($arraygoals, $g);
		}
		
		return $arraygoals;
	}
	public function GetInversionPorGoal($goal){
		$query = $this->connect()->prepare('SELECT * FROM depositos WHERE goal = :goal');
		$query->execute(['goal' => $goal]);
		$monto = 0;
		foreach ($query as $g) {
			$monto = $monto + $g[2];
		}
		return $monto;
	}
	public function GetDepositos(){
		$query = $this->connect()->prepare('SELECT * FROM depositos');
		$query->execute();
		$arraydatos = array();
		foreach ($query as $d) {
			
			
			array_push($arraydatos, $d);
		}
		return $arraydatos;
	}
	public function SetDeposito($goal, $monto, $fecha){
		try {
		$query = $this->connect()->prepare('INSERT INTO depositos VALUES(null, :goal, :monto, :fecha)');
		$query->execute(['goal' => $goal, 'monto' => $monto, 'fecha' => $fecha]);
		} catch (PDOException $e) {
		print_r('Error conenection: ' . $e->getMessage());
		
	}

	}

}



$insertar = new DB();
$bol = $insertar->SetDatos();

if($bol){
	echo '<script type="text/javascript">
	alert("Datos ingresados");
  </script>';
}



?>