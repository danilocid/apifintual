<?php
include 'controller.php';
$dat = new DB();
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/dashboard.css">
    <link rel="stylesheet" type="text/css" href="DataTables/datatables.min.css"/>
    <title>Api Fintual V2 - APV</title>
</head>
<body>


<nav class="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow">
  <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="#">Api Fintual</a>
  
</nav>

<div class="container-fluid">
  <div class="row">
    <nav class="col-md-2 d-none d-md-block bg-light sidebar">
      <div class="sidebar-sticky">
        <ul class="nav flex-column">
            <li class="nav-item">
              <a class="nav-link" href="index">
                <span data-feather="home"></span>
                Resumen <span class="sr-only"></span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link " href="casanueva">
                <span data-feather="shopping-cart"></span>
                Casa nueva
              </a>
            </li>
            </li>
            <li class="nav-item">
              <a class="nav-link active" href="apv">
                <span data-feather="shopping-cart">(current)</span>
               APV
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="depositos">
                <span data-feather="shopping-cart"></span>
               Depositos
              </a>
            </li>
            
          </ul>

        
        
      </div>
    </nav>

    <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
      <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Resumen</h1>
        <div class="btn-toolbar mb-2 mb-md-0">
          <div class="btn-group mr-2">
           
            <button type="button" class="btn btn-sm btn-outline-secondary" data-toggle="modal" data-target="#modal-default">Verificar</button>
          </div>
         
        </div>
      </div>

      <canvas class="my-4 w-100" id="myChart" width="900" height="380"></canvas>

      <h2>Detalle</h2>
      <div class="table-responsive">
        <table class="table table-bordered table-striped" id="apv">
          <thead>
            <tr>
              <th>Id</th>
              <th>Inversion</th>
              <th>Monto</th>
              <th>Invertido</th>
              <th>Ganancia</th>
              <th>% ganancia</th>
              <th>Diferencia</th>
              <th>Ganancia promedio</th>
              <th>Fecha</th>
            </tr>
          </thead>
          <tbody>
          <?php
              
              
              $data = $dat->GetDatosAPV();
              $datag = $dat->GetDatosAPVGraficos();
                 
              $anterior = 0;
              $contador = 0;
              $promedio = 0;
              $suma = 0;
              $invertidostring = "";
              $montostring = "";
              $fechas = "";
                foreach ($data as $d) {
                  $diferencia = (($d['Monto']-$d['invertido'])-$anterior);
                  if ($diferencia != 0) {
                  $contador = $contador + 1;
                  $suma = $suma + ($d['Monto']-$d['invertido']);
                  $promedio = $suma / $contador;
                echo '<tr>
                <td>('.$contador.') '.$d['Id'].'</td>
                <td>'.$dat->GetGoalPorId($d['goal']).'</td>
                <td> $'.number_format($d['Monto'], 0, '.', ',').'</td>
                <td> $'.number_format($d['invertido'], 0, '.', ',').'</td>
                <td>'.number_format(($d['Monto']-$d['invertido']), 0, '.', ',').'</td>
                <td>'.number_format(((($d['Monto']/$d['invertido'])-1)*100), 3, '.', ',').' %</td>';
                
                if ($diferencia <= 0) {
                    echo  '<td style="color:red">$'.number_format($diferencia, 0, '.', ',').'</td>';
                } else {
                    echo  '<td  style="color:blue">$'.number_format($diferencia, 0, '.', ',').'</td>';
                }
                echo '<td> $'.number_format($promedio, 1, '.', ',').'</td>';
                
               
                echo '<td>'.$d['Fecha'].'</td>';
                                    
                      echo '</tr>
                      ';
                  $anterior = ($d['Monto']-$d['invertido']);
                }
               if ($contador == 60) {
                 break;
               }
               
              }
              foreach ($datag as $d) {
                
                $montostring = $montostring . "'".number_format($d['Monto'], 0, ',', '')."'".',';
                $invertidostring = $invertidostring . "'".number_format($d['invertido'], 0, ',', '')."'".',';
                $fechas = $fechas . "'".(date("d-m-Y",strtotime($d['Fecha']))."'".',');
              
             
            }
              $fechas = trim($fechas, ',');
              $montostring = trim($montostring, ',');
              $invertidostring = trim($invertidostring, ',');
                ?>
            
          </tbody>
        </table>
      </div>
    </main>
  </div>
</div>
<div class="modal fade" id="modal-default">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        
                        <h4 class="modal-title">Verificar APV</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span></button>
                      </div>
                      <div class="modal-body">
                        <form action="AgregarAPV.php" method="POST">        
                              <?php
                              $datos = $dat->GetDatosAPVfromAPI();
                              $monto = $datos['data']['attributes']['nav'];
                              $invertido = $dat->GetInversionPorGoal($datos['data']['id']);
                              $diferencia = (($monto-$invertido)-$anterior);
                              //$diferencia = $monto - 
                             
                              ?>
                            
                            <div class ="form-group">
                              <label>Monto</label>
                              <input id="monto" name="monto" value=<?php echo $monto ?> required type="number" class="form-control input-sm">
                            </div>
                            <div class ="form-group">
                              <label>Invertido</label>
                              <input id="invertido" name="invertido" value=<?php echo $invertido ?> required type="number" class="form-control input-sm">
                            </div>
                            <div class ="form-group">
                              <label>Diferencia</label>
                              <input id="diferencia" name="diferencia" value=<?php echo $diferencia ?> required type="number" class="form-control input-sm">
                            </div>
                       
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary">Agregar datos</button>
                      </div>
                      </form>
                    </div>



    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="js/bootstrap.js"></script>
    <script type="text/javascript" src="DataTables/datatables.min.js"></script>
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/feather-icons/4.9.0/feather.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.min.js"></script>
    <script>
      
  $(function () {
   
   $('#apv').DataTable({
     'paging'      : true,
     'lengthChange': false,
     'searching'   : false,
     'ordering'    : true,
     'info'        : true,
     'autoWidth'   : true,
     "order": [[ 8, "desc" ]]
   });
  })
</script>
    <script>
        /* globals Chart:false, feather:false */

(function () {
  'use strict'

  feather.replace()

  // Graphs
  var ctx = document.getElementById('myChart')
  // eslint-disable-next-line no-unused-vars
  var myChart = new Chart(ctx, {
    type: 'line',
    data: {
      labels: [
        <?php
        echo $fechas;
        ?>
      ],
      datasets: [{
        label: 'Inveritdo',
        data: [
          <?php
        echo $invertidostring;
        ?>
        ],
        lineTension: 0,
        backgroundColor: 'transparent',
        borderColor: '#007bff',
        borderWidth: 4,
        pointBackgroundColor: '#007bff'},
        {
        label: 'Monto',
        data: [
          <?php
        echo $montostring;
        ?>
        ],
        lineTension: 0,
        backgroundColor: 'transparent',
        borderColor: '#070bff',
        borderWidth: 4,
        pointBackgroundColor: '#007bff'
      }]
    },
    options: {
      scales: {
        yAxes: [{
          ticks: {
            beginAtZero: false
          }
        }]
      },
      legend: {
        display: false
      }
    }
  })
}())
    </script>
    
</body>

</html>
