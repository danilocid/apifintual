<?php

include 'controller.php';
$dat = new DB();
$data = $dat->GetDatos();
//print_r($data);
$anterior = 0;
$suma = 0;
$contador = 0;

             echo "<table>";
                foreach ($data as $d) {
                  $diferencia = (($d->monto-$d->invertido)-$anterior);
                  if ($diferencia != 0) {
                  $contador = $contador + 1;
                  $suma = $suma + ($d->monto-$d->invertido);
                  $promedio = $suma / $contador;
                echo '<tr>
                <td>'.$d->id.'</td>
                <td>'.$dat->GetGoalPorId($d->goal).'</td>
                <td> $'.number_format($d->monto, 0, ',', '.').'</td>
                <td> $'.number_format($d->invertido, 0, ',', '.').'</td>
                <td>'.number_format(($d->monto-$d->invertido), 0, ',', '.').'</td>
                <td>'.number_format(((($d->monto/$d->invertido)-1)*100), 3, ',', '.').' %</td>';
                
                if ($diferencia <= 0) {
                    echo  '<td style="color:red">$'.number_format($diferencia, 0, ',', '.').'</td>';
                } else {
                    echo  '<td  style="color:blue">$'.number_format($diferencia, 0, ',', '.').'</td>';
                }
                echo '<td> $'.number_format($promedio, 1, ',', '.').'</td>';
                
               
                echo '<td>'.$d->fecha.'</td>';
                                    
                      echo '</tr>
                      ';
                  $anterior = ($d->monto-$d->invertido);
                }
                
              }
             
                ?>