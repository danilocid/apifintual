<?php
class Datos{
	public $id;
	public $goal;
	public $monto;
	public $invertido;
	public $fecha;
	

}
class DB
{
	private $host;
	private $db;
	private $user;
	private $password;

	public function __construct()
	{
		$this->host = 'localhost';
		$this->db = 'fintualv2';
		$this->user = 'root';
		$this->password = '';
	}

	public function connect()
	{
		try {
			$connection = "mysql:host=" . $this->host . ";dbname=" . $this->db;
			$options = [
				PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
				PDO::ATTR_EMULATE_PREPARES => false,
			];
			$pdo = new PDO($connection, $this->user, $this->password, $options);

			return $pdo;
		} catch (PDOException $e) {
			print_r('Error conenection: ' . $e->getMessage());
		}
    }
    
    public function SetDatos(){
		$query = $this->connect()->prepare('SELECT max(Fecha) FROM datos');
		$query->execute([]);
		foreach ($query as $a) {
			$max = $a[0];
			
		}
		$fecha = date("Y-m-d");
        if ($fecha == $max) {
           
        } else {

        $json = file_get_contents("https://fintual.cl/api/goals?user_token=EDFyJxckhKysafPWPzzD&user_email=danilo.cid.v%40gmail.com");
		$datos = json_decode($json, true);
 

		foreach ($datos as $d) {
		$id1 = $d[0]['id'];
		$monto1 = $d[0]['attributes']['nav'];
		$invertido1 = $this->GetInversionPorGoal($id1);
		$id2 = $d[1]['id'];
		$monto2 = $d[1]['attributes']['nav'];
		$invertido2 = $this->GetInversionPorGoal($id2);
		}
		try {
            $query = $this->connect()->prepare('INSERT into datos values(null,:id1, :monto1, :invertido1, now()),(null,:id2, :monto2, :invertido2, now());');
			$query->execute(['id1' => $id1, 'monto1' => $monto1, 'invertido1' => $invertido1, 'id2' => $id2, 'monto2' => $monto2, 'invertido2' => $invertido2]);
            return true;
        } catch (PDOException $e) {
			print_r('Error conenection: ' . $e->getMessage());
            return false;
		}
	}
	}
	public function GetDatosAPVfromAPI(){
		$json = file_get_contents("https://fintual.cl/api/goals/41679?user_token=EDFyJxckhKysafPWPzzD&user_email=danilo.cid.v%40gmail.com");
		$datos = json_decode($json, true);
		return $datos;
	}
	public function GetDatosCasaNuevafromAPI(){
		$json = file_get_contents("https://fintual.cl/api/goals/27362?user_token=EDFyJxckhKysafPWPzzD&user_email=danilo.cid.v%40gmail.com");
		$datos = json_decode($json, true);
		return $datos;
	}
	public function SetDatosAPV(){
		

        $json = file_get_contents("https://fintual.cl/api/goals/41679?user_token=EDFyJxckhKysafPWPzzD&user_email=danilo.cid.v%40gmail.com");
		$datos = json_decode($json, true);
 

		
		$id = $datos['data']['id'];
		$monto = $datos['data']['attributes']['nav'];
		$invertido = $this->GetInversionPorGoal($id);
			
		try {
            $query = $this->connect()->prepare('INSERT into apv values(null,:id, :monto, :invertido, now());');
			$query->execute(['id' => $id, 'monto' => $monto, 'invertido' => $invertido]);
            return true;
        } catch (PDOException $e) {
			print_r('Error conenection: ' . $e->getMessage());
            return false;
		}
	
	}
	public function SetDatosCasaNueva(){
		

        $json = file_get_contents("https://fintual.cl/api/goals/27362?user_token=EDFyJxckhKysafPWPzzD&user_email=danilo.cid.v%40gmail.com");
		$datos = json_decode($json, true);
 

		
		$id = $datos['data']['id'];
		$monto = $datos['data']['attributes']['nav'];
		$invertido = $this->GetInversionPorGoal($id);
			
		try {
            $query = $this->connect()->prepare('INSERT into casanueva values(null,:id, :monto, :invertido, now());');
			$query->execute(['id' => $id, 'monto' => $monto, 'invertido' => $invertido]);
            return true;
        } catch (PDOException $e) {
			print_r('Error conenection: ' . $e->getMessage());
            return false;
		}
	
	}
	
	public function GetDatos(){
		$query = $this->connect()->prepare('SELECT apv.Fecha from apv UNION SELECT casanueva.Fecha from casanueva ORDER BY fecha DESC LIMIT 60');
		$query->execute();
		$arrayfechas = array();
		foreach($query as $f){
			array_push($arrayfechas, $f);
		}
		$array = array();
		$montoold = 0;
		$invertidoold = 0;
		$montoold2 = 0;
		$invertidoold2 = 0;
		foreach ($arrayfechas as $f){
			$query = $this->connect()->prepare('SELECT * FROM casanueva WHERE fecha = :fecha');
			$query->execute(['fecha' => $f[0]]);
			$rows = $query->fetchAll();
			$query->execute(['fecha' => $f[0]]);
			$datos = new Datos();
			if (count($rows) == "1") {
				foreach ($query as $d) {
					
					$datos->id = $d[0];
					$datos->goal = $d[1];
					$datos->monto = $d[2];
					$datos->invertido = $d[3];
					$datos->fecha = $f[0];
					array_push($array, $datos);
					$montoold = $d[2];
					$invertidoold = $d[3];
					
				}
			} else {
				
				
				$datos->id = 0;
				$datos->goal = 27362;
				$datos->monto = $montoold;
				$datos->invertido = $invertidoold;
				$datos->fecha = $f[0];
				array_push($array, $datos);
			}
			$query = $this->connect()->prepare('SELECT * FROM apv WHERE fecha = :fecha');
			$query->execute(['fecha' => $f[0]]);
			$rows = $query->fetchAll();
			$query->execute(['fecha' => $f[0]]);
			$datos = new Datos();
			if (count($rows) == "1") {
				foreach ($query as $d) {
					
					$datos->id = $d[0];
					$datos->goal = $d[1];
					$datos->monto = $d[2];
					$datos->invertido = $d[3];
					$datos->fecha = $f[0];
					array_push($array, $datos);
					$montoold2 = $d[2];
					$invertidoold2 = $d[3];
					
				}
			} else {
				
				
				$datos->id = 0;
				$datos->goal = 41679;
				$datos->monto = $montoold2;
				$datos->invertido = $invertidoold2;
				$datos->fecha = $f[0];
				array_push($array, $datos);
			}
			
			
		}
	
	return $array;

		
	}
	public function GetDatosAPV(){
		$query = $this->connect()->prepare('SELECT * FROM apv ORDER BY Fecha desc LIMIT 60 ');
		$query->execute();
		$arraydatos = array();
		foreach ($query as $d) {
			$datos = new Datos;
			$datos->id = $d[0];
			$datos->goal = $d[1];
			$datos->monto = $d[2];
			$datos->fecha = $d[3];
			$datos->invertido = $d[4];
			
			array_push($arraydatos, $d);
		}
		return array_reverse($arraydatos);
	}
	public function GetDatosAPVGraficos(){
		
		$query = $this->connect()->prepare('SELECT * FROM apv ORDER BY fecha DESC LIMIT 60');
		$query->execute();
		$arraydatos = array();
		foreach ($query as $d) {
			$datos = new Datos;
			$datos->id = $d[0];
			$datos->goal = $d[1];
			$datos->monto = $d[2];
			$datos->fecha = $d[3];
			$datos->invertido = $d[4];
			
			array_push($arraydatos, $d);
		}
		return $arraydatos;
	}
	public function GetDatosCasaNueva(){
		$query = $this->connect()->prepare('SELECT * FROM casanueva ORDER BY fecha DESC LIMIT 60');
		$query->execute();
		$arraydatos = array();
		foreach ($query as $d) {
			$datos = new Datos;
			$datos->id = $d[0];
			$datos->goal = $d[1];
			$datos->monto = $d[2];
			$datos->fecha = $d[3];
			$datos->invertido = $d[4];
			
			array_push($arraydatos, $d);
		}
		return array_reverse($arraydatos);
	}
	public function GetDatosCasaNuevaGraficos(){
		
		$query = $this->connect()->prepare('SELECT * FROM casanueva ORDER BY fecha DESC LIMIT 60');
		$query->execute();
		$arraydatos = array();
		foreach ($query as $d) {
			$datos = new Datos;
			$datos->id = $d[0];
			$datos->goal = $d[1];
			$datos->monto = $d[2];
			$datos->fecha = $d[3];
			$datos->invertido = $d[4];
			
			array_push($arraydatos, $d);
		}
		return $arraydatos;
	}
	
	public function GetGoalPorId($goal){
		$query = $this->connect()->prepare('SELECT * FROM goals WHERE goal = :goal');
		$query->execute(['goal' => $goal]);
		foreach ($query as $g) {
			$nombre = $g[2];
		}
		return $nombre;
	}
	public function GetGoals(){
		$query = $this->connect()->prepare('SELECT * FROM goals');
		$query->execute();
		$arraygoals = array();
		foreach ($query as $g) {
			array_push($arraygoals, $g);
		}
		
		return $arraygoals;
	}
	public function GetInversionPorGoal($goal){
		$query = $this->connect()->prepare('SELECT * FROM depositos WHERE goal = :goal');
		$query->execute(['goal' => $goal]);
		$monto = 0;
		foreach ($query as $g) {
			$monto = $monto + $g[2];
		}
		return $monto;
	}
	public function GetDepositos(){
		$query = $this->connect()->prepare('SELECT DISTINCT  fecha FROM depositos');
		$query->execute();
		$arrayfechas = array();
		foreach ($query as $d) {
			
			
			array_push($arrayfechas, $d);
		}
		//obtenemos las fechas
		$array = array();
		foreach ($arrayfechas as $f){
				$query = $this->connect()->prepare('SELECT * FROM depositos WHERE fecha = :fecha AND goal = 27362');
				$query->execute(['fecha' => $f[0]]);
				$rows = $query->fetchAll();
				$query->execute(['fecha' => $f[0]]);
				$datos = new Datos();
				if (count($rows) == "1") {
					foreach ($query as $d) {
						
						$datos->id = $d[0];
						$datos->goal = $d[1];
						$datos->monto = $d[2];
						$datos->fecha = $d[3];
						array_push($array, $datos);
						
						
					}
				} else {
					
					
					$datos->id = 0;
					$datos->goal = 27362;
					$datos->monto = 0;
					$datos->fecha = $f[0];
					array_push($array, $datos);
				}
				$query = $this->connect()->prepare('SELECT * FROM depositos WHERE fecha = :fecha AND goal = 41679');
				$query->execute(['fecha' => $f[0]]);
				$rows = $query->fetchAll();
				$query->execute(['fecha' => $f[0]]);
				$datos = new Datos();
				if (count($rows) == "1") {
					foreach ($query as $d) {
						
						$datos->id = $d[0];
						$datos->goal = $d[1];
						$datos->monto = $d[2];
						$datos->fecha = $d[3];
						array_push($array, $datos);
						
						
					}
				} else {
					
					
					$datos->id = 0;
					$datos->goal = 41679;
					$datos->monto = 0;
					$datos->fecha = $f[0];
					array_push($array, $datos);
				}
				
				
			}
		
		return $array;
	}
	public function SetDeposito($goal, $monto, $fecha){
		try {
		$query = $this->connect()->prepare('INSERT INTO depositos VALUES(null, :goal, :monto, :fecha)');
        $query->execute(['goal' => $goal, 'monto' => $monto, 'fecha' => $fecha]);
        echo '<script type="text/javascript">
            window.location="depositos.php";
            </script>';
		} catch (PDOException $e) {
		print_r('Error conenection: ' . $e->getMessage());
		
	}

	}

}







?>