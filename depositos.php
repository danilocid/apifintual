<?php
include 'controller.php';
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/dashboard.css">
    <link rel="stylesheet" href="css/bootstrap-datepicker.min.css">
    <title>Api Fintual V2 - Depositos</title>
</head>
<body>


<nav class="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow">
  <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="#">API Fintual</a>
  
</nav>

<div class="container-fluid">
  <div class="row">
    <nav class="col-md-2 d-none d-md-block bg-light sidebar">
      <div class="sidebar-sticky">
        <ul class="nav flex-column">
            <li class="nav-item">
              <a class="nav-link" href="index">
                <span data-feather="home"></span>Resumen <span class="sr-only"></span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link " href="casanueva">
                <span data-feather="shopping-cart"></span>
                Casa nueva
              </a>
            </li>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="apv">
                <span data-feather="shopping-cart"></span>
               APV
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link active" href="depositos">
                <span data-feather="shopping-cart">(current)</span>
               Depositos
              </a>
            </li>
            
          </ul>

        
        
      </div>
    </nav>

    <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
      <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Historial</h1>
        <div class="btn-group mr-2">
            <button type="button" class="btn btn-sm btn-outline-secondary" data-toggle="modal" data-target="#modal-default" >Agregar deposito</button>
            
          </div>
        
      </div>

      <canvas class="my-4 w-100" id="myChart" width="900" height="380"></canvas>

      <h2>Detalle</h2>
      <div class="table-responsive">
        <table  class="table table-bordered table-striped" id="apv">
          <thead>
            <tr>
              
              <th>Inversion</th>
              <th>Monto</th>
              <th>Inversion</th>
              <th>Monto</th>
              <th>Fecha</th>
              
            </tr>
          </thead>
          <tbody>
          <?php
            class DatosOrdenados
            {
              public $id;
              public $goal;
              public $monto;
              public $id2;
              public $goal2;
              public $monto2;
              public $fecha;
                  }
           $fechas = "";
           $apv = 0;
           $casanueva = 0;
           $apvstring = "";
           $casanuevastring = "";
              $dat = new DB();
              $data = $dat->GetDepositos();
              setlocale(LC_TIME, 'es_ES.UTF-8');
             // print_r($data);
              $contador = count($data);
              $datosordenados  = new DatosOrdenados();
              $datosord = array();
              for ($i=0; $i < $contador; $i = $i + 2) { 
                $datosordenados  = new DatosOrdenados();
                $datosordenados->id = $data[$i]->id;
                $datosordenados->goal = $data[$i]->goal;
                $datosordenados->monto = $data[$i]->monto;
                $datosordenados->id2 = $data[$i+1]->id;
                $datosordenados->goal2 = $data[$i+1]->goal;
                $datosordenados->monto2 = $data[$i+1]->monto;
                $datosordenados->fecha = $data[$i]->fecha;
                array_push($datosord, $datosordenados);
              }
              //print_r($datosord);
              foreach ($datosord as $d) {
               
              echo '<tr>
                    
                    <td>'.$dat->GetGoalPorId($d->goal).'</td>
                    <td> $'.number_format($d->monto, 0, ',', '.').'</td>
                    
                    <td>'.$dat->GetGoalPorId($d->goal2).'</td>
                    <td> $'.number_format($d->monto2, 0, ',', '.').'</td>
                    
                    <td>'.date("d-m-Y",strtotime($d->fecha)).'</td>';
                   
                                  
                    echo '</tr>
                    ';
               $fechas = $fechas . "'".(date("d-m-Y",strtotime($d->fecha))."'".',');
             
               
                $apv = $apv + $d->monto2;
                $apvstring = $apvstring . "'".number_format($apv, 0, ',', '')."'".',';
                
               
                $casanueva = $casanueva + $d->monto;
                $casanuevastring = $casanuevastring . "'".number_format($casanueva, 0, ',', '')."'".',';
                
               
               
              
               
              }
              ?>
            
          </tbody>
        </table>
        <?php
        $fechas = trim($fechas, ',');
        $casanuevastring = trim($casanuevastring, ',');
        $apvstring = trim($apvstring, ',');
        
        ?>
      </div>
    </main>
  </div>
</div>
<div class="modal fade" id="modal-default">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        
                        <h4 class="modal-title">Agregar deposito</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span></button>
                      </div>
                      <div class="modal-body">
                        <form action="AgregarDeposito.php" method="POST">        
                              
                            <div class="form-group">
                              <label>Goal</label>
                              <select id="goal" name="goal" class="form-control input-sm">
                                <?php
                                $goals = $dat->GetGoals();
                                foreach ($goals as $g) {
                                    echo '<option value="'.$g['goal'].'">'.$g['nombre'].'</option>';
                                }
                                ?>
                              </select>
                            </div> 
                            <div class ="form-group">
                              <label>Monto</label>
                              <input id="monto" name="monto" required type="number" class="form-control input-sm">
                            </div>
                            <div class="form-group">
                                <label>Fecha:</label>
                
                                <div class="input-group date">
                                  <div class="input-group-addon input-sm">
                                    <i class="fa fa-calendar"></i>
                                  </div>
                                  <input type="text" class="form-control pull-right input-sm" id="datepicker" name="datepicker" required>
                                </div>
                                <!-- /.input group -->
                              </div>            
                                  
                            
                        
                        
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary">Agregar deposito</button>
                      </div>
                      </form>
                    </div>


    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="js/bootstrap.js"></script>
    <script src="js/bootstrap-datepicker.min.js"></script>
    <script src="js/bootstrap-datepicker.es.min.js"></script>
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/feather-icons/4.9.0/feather.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.min.js"></script>
    <script>
        /* globals Chart:false, feather:false */

(function () {
  'use strict'

  feather.replace()

  // Graphs
  var ctx = document.getElementById('myChart')
  // eslint-disable-next-line no-unused-vars
  var myChart = new Chart(ctx, {
    type: 'line',
    data: {
      labels: [
        <?php
        echo $fechas;
        ?>
      ],
      datasets: [{
        label: 'APV',
        data: [
          <?php
        echo $apvstring;
        ?>
        ],
        lineTension: 0,
        backgroundColor: 'transparent',
        borderColor: '#007bff',
        borderWidth: 4,
        pointBackgroundColor: '#007bff'},
        {
        label: 'Casa Nueva',
        data: [
          <?php
        echo $casanuevastring;
        ?>
        ],
        lineTension: 0,
        backgroundColor: 'transparent',
        borderColor: '#070bff',
        borderWidth: 4,
        pointBackgroundColor: '#007bff'
      }]
    },
    options: {
      scales: {
        yAxes: [{
          ticks: {
            beginAtZero: false
          }
        }]
      },
      legend: {
        display: false
      }
    }
  })
}())
    </script>
    <script>
  $(function () {
   
   
  
   
   $('#datepicker').datepicker({
    
        format: "yyyy/mm/dd",
        clearBtn: true,
        language: "es",
        autoclose: true,
        keyboardNavigation: false,
        todayHighlight: true
    });              
   
 })
</script>
<script>
      
  $(function () {
   
   $('#apv').DataTable({
     'paging'      : true,
     'lengthChange': false,
     'searching'   : false,
     'ordering'    : true,
     'info'        : true,
     'autoWidth'   : true,
     "order": [[ 8, "desc" ]]
   });
  })
</script>
</body>
</html>
